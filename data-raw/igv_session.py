"""Populate IGV session with bedgraph files to save XML.

Once we save the IGV XML session file we no longer need this script.

It's not possible to fit all 196 files in 16 GB of RAM.  IGV's default
4G of memory will max out at 76 files.  One can increase -Xmx4g to
-Xmx10g in igv.sh

But here we filter for largest files.

"""

import itertools
import logging
import socket
import pathlib

IP = ('127.0.0.1', 60151, )
BUFFER_SIZE = 1024


def run_factory(connection):
    """Return socket send/receive function for socket object."""

    def func(command):
        connection.sendall(str.encode('{}\r'.format(command)))
        return connection.recv(BUFFER_SIZE)

    return func


def sort_keys(path):            # pylint: disable=redefined-outer-name
    """Return tuple of keys to sort by.

    Example file name:
    GSM3004723_51634_R1_cap.clip.ATCACG.hs37d5.bwa.uniqueUMI.pl.bedgraph.gz

    Sort keys: (ATCACG, 51634, 0)

    """
    umi_prefix = '.clip.'
    idx = path.name.find(umi_prefix) + len(umi_prefix)
    umi = path.name[idx:idx+6]
    id_prefix = '_'
    idx = path.name.find(id_prefix) + len(id_prefix)
    id_ = int(path.name[idx:idx+5])
    is_minus = path.name.find('.mn.') != -1
    return (umi, id_, int(is_minus), )


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)

    # pylint: disable=invalid-name
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect(IP)
        run = run_factory(conn)
        # Reset tracks.
        data = run('new')
        # Reset zoom.
        data = run('goto all')
        # Load bedgraphs of these accessions.
        path_parent = pathlib.Path('.')
        accessions = ['GSM3004654', 'GSM3004720']
        globs = ['GSE*/*{}*.gz'.format(accession) for accession in accessions]
        paths = [list(path_parent.glob(glob)) for glob in globs]
        # Flatten out the lists.
        paths = list(itertools.chain(*paths))
        # Sort by UMI, ID then strand.
        paths = sorted(paths, key=sort_keys)
        for i, path in enumerate(paths):
            logging.info('Loading file %3d : %s', i + 1, path.name)
            run('load {}'.format(str(path.absolute())))
